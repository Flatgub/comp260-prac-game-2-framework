﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class movePaddle : MonoBehaviour {
    private Rigidbody physBody;

    public float speed = 20.0f;

	// Use this for initialization
	void Start () {
        physBody = GetComponent<Rigidbody>();
	}


    private void FixedUpdate() {
        Vector3 targetPos = GetMousePosition();
        Vector3 dirToPos = targetPos - physBody.position ;
        Vector3 velocity = dirToPos.normalized * speed;

        float distToMove = speed * Time.fixedDeltaTime;
        float distToPos = dirToPos.magnitude;

        if (distToMove > distToPos) {
            velocity = velocity * distToPos / distToMove;
        }

        physBody.velocity = velocity;

    }


    private Vector3 GetMousePosition() {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
