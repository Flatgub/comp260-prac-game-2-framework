﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class movePaddleKeyboard : MonoBehaviour {
    private Rigidbody physBody;

    public float speed = 20.0f;

	// Use this for initialization
	void Start () {
        physBody = GetComponent<Rigidbody>();
	}


    private void FixedUpdate() {

        Vector3 dirToPos = GetKeyboardVector();

        if (dirToPos.magnitude < 0.1f) { //no input is present, decelerate rapidly
            physBody.velocity = physBody.velocity * 0.5f;
        } 
        else 
        { //inputs are present, add forces proportional to the "keyboard joystick"

            Vector3 currentDir = physBody.velocity.normalized;
            float dot = Vector3.Dot(currentDir, dirToPos);

            float speedBoost = Mathf.Clamp(dot, -1, 0) * -1; // the range 0 to 1 representing how hard the player is entering reverse inputs

            float outputSpeed = speed + (speed * 3 * speedBoost); //when reversing, allow up to quadruple speed for quick course correction;

            Vector3 velocity = dirToPos.normalized * outputSpeed;

            physBody.AddForce(velocity);
        }
        

    }


    private Vector3 GetKeyboardVector() {
        float hori = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 outVec = new Vector3(hori,0,vert);

        return outVec.normalized;

    }

 
}
