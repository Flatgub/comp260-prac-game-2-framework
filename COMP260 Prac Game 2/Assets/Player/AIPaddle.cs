﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AIPaddle : MonoBehaviour {
    private Rigidbody physBody;

    public Transform player;
    public Transform puck;
    public Transform goal;

    public float speed = 20.0f;
    public float minGoalDistance = 3.0f;
    public float maxGoalDistance = 10.0f;

	// Use this for initialization
	void Start () {
        physBody = GetComponent<Rigidbody>();

	}


    private void FixedUpdate() {
        Vector3 targetPos = GetTargetPosition();
        Vector3 dirToPos = targetPos - physBody.position ;
        Vector3 velocity = dirToPos.normalized * speed;

        float distToMove = speed * Time.fixedDeltaTime;
        float distToPos = dirToPos.magnitude;

        if (distToMove > distToPos) {
            velocity = velocity * distToPos / distToMove;
        }

        physBody.velocity = velocity;

    }


    private Vector3 GetTargetPosition() {
        Vector3 puckPos = puck.position;
        Vector3 goalPos = goal.position;

        Vector3 goalToMe = transform.position - goalPos;
        Vector3 goalToPuck = puckPos - goalPos;

        Vector3 vecToPuck = puckPos - transform.position;
        Vector3 targetPos;

        //hit the puck if its close (if its close, and if i'm close to the goal, and its further away from the goal than me)
        if (vecToPuck.magnitude < 2 && goalToMe.magnitude < maxGoalDistance*0.75 && puckPos.x < 4) {
            targetPos = (vecToPuck * 2) + transform.position;
        }
        else { //the puck is far, move to defensive position

            if (goalToPuck.magnitude < minGoalDistance) {
                goalToPuck = goalToPuck.normalized * minGoalDistance;
            } else if (goalToPuck.magnitude > maxGoalDistance) {
                goalToPuck = goalToPuck.normalized * maxGoalDistance;
            }

            targetPos = goalPos + goalToPuck * 0.5f;

        }

        

        return targetPos;
    }
}
