﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class PuckControl : MonoBehaviour {

    public AudioClip collideWallSound;
    public AudioClip collidePaddleSound;
    public LayerMask paddleLayer;

    private Vector3 startingPos;
    private Rigidbody physBody;


    private AudioSource audio;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        physBody = GetComponent<Rigidbody>();
        startingPos = transform.position;
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (paddleLayer.Contains(collision.gameObject)) {
            audio.PlayOneShot(collidePaddleSound);
        } else {
            audio.PlayOneShot(collideWallSound);
        }
    }

    public void ResetPos()
    {
        physBody.position = startingPos;
        physBody.velocity = Vector3.zero;
    }
}
