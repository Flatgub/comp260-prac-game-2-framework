﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GoalController : MonoBehaviour {

	public int player;

    public AudioClip scoreSound;
    private AudioSource audio;



	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	
    private void OnTriggerEnter(Collider other)
    {
        audio.PlayOneShot(scoreSound);

		Scorekeeper.Instance.OnScoreGoal(player);

        PuckControl puck = other.gameObject.GetComponent<PuckControl>();
		if (!Scorekeeper.Instance.isGameOver()) {
			puck.ResetPos();
		} else {
			//the game is over, hide the puck
			puck.gameObject.SetActive(false);
		}

    }
}
