﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;
	static private bool gameIsOver = false;

	static public Scorekeeper Instance {
		get { return instance; }
	}

    public int pointsPerGoal = 1;
    private int[] score = new int[2];

	public int scoreToWin = 10;
	public Text[] scoreText;
	public Text winnerText;

	void Start () {
		if (instance == null) {
			instance = this;

			for (int i = 0; i < 2; i++) { 
				score[i] = 0;
				scoreText[i].text = "0";
			}

			winnerText.enabled = false;

        } else {
            Debug.LogError("More than one scorekeeper exists in the scene");
        }
	}

	
	   
    // Update is called once per frame
	void Update () {
		
	}

	public bool isGameOver()
	{
		return gameIsOver;
	}

    public void OnScoreGoal(int player)
    {
		score[player] += pointsPerGoal;
		scoreText[player].text = score[player].ToString();

		if (score[player] == scoreToWin) { //end the game at a victory
			gameIsOver = true;
			winnerText.enabled = true;
			winnerText.text = string.Format("PLAYER {0} WINS",player+1);
			}
    }
}
